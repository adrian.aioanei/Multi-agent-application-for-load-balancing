﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;


namespace Reactive
{
    class mainProg
    {
        static void Main(string[] args)
        {

            var env = new ActressMas.Environment();
            ProcessorAgent p1 = new ProcessorAgent();
            ProcessorAgent p2 = new ProcessorAgent();
            ProcessorAgent p3 = new ProcessorAgent();
            ProcessorAgent p4 = new ProcessorAgent();

            Distribuitor dis = new Distribuitor();
            Dispatcher disp = new Dispatcher();

            env.Add(p1, "ProcessorAgent0");
            env.Add(p2, "ProcessorAgent1");
            env.Add(p3, "ProcessorAgent2");
            env.Add(p4, "ProcessorAgent3");

            env.Add(dis, "Distribuitor");
            env.Add(disp, "Dispatcher");

            p1.Start();
            p2.Start();
            p3.Start();
            p4.Start();

            dis.Start();
            disp.Start();

            env.WaitAll();
        }
    }
}
