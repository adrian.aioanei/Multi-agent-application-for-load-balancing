﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Reactive
{
    class Utils
    {
        public static int Delay = 100;
        public static Random RandNoGen = new Random();

        public static bool EventOccurs(float probability)
        {
            return RandNoGen.NextDouble() <= probability;
        }

        public static void ParseMessage(string content, out string action, out string parameters)
        {
            string[] t = content.Split();

            action = t[0];

            parameters = "";

            if (t.Length > 1)
            {
                for (int i = 1; i < t.Length - 1; i++)
                    parameters += t[i] + " ";
                parameters += t[t.Length - 1];
            }
        }

        public static string Str(string p1, double p2)
        {
            return string.Format("{0} {1:F1}", p1, p2);
        }
    }
}
