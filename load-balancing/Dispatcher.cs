﻿using ActressMas;
using System;

namespace Reactive
{
    public class Dispatcher : Agent
    {
        private static string AGENT = "ProcessorAgent";
        public override void Act(Message message)
        {
            string action, parameters;
            Utils.ParseMessage(message.Content, out action, out parameters);

            Console.WriteLine("{0} received from {1} : {2}", this.Name, message.Sender, action);

            Send(getAnotherAgentProcessor(message.Sender), action);

        }

        private string getAnotherAgentProcessor(string sender)
        {
            int agentNumber = (getLastCharacter(sender));
            if (agentNumber == 3)
                return AGENT + "0";
            else
                return AGENT + (agentNumber + 1);

        }

        private int getLastCharacter(string agentName)
        {
            return (int)Char.GetNumericValue(agentName[agentName.Length - 1]);
        }

    }
}
