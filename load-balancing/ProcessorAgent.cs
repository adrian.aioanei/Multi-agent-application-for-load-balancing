﻿using System;
using System.Threading;
using ActressMas;


namespace Reactive
{
    public class ProcessorAgent
                 : Agent
    {
        private string path;
        Int64 sizeToProcess;
        private System.IO.FileStream fileStream;
        private int fileOffset;
        private static Mutex mut = new Mutex();
        bool flag = true;

        public ProcessorAgent()
        {
            flag = true;
        }

        public ProcessorAgent(string path, uint sizeToProcess)
        {
            this.path = path;
            this.sizeToProcess = sizeToProcess;
        }

        public override void Act(Message message)
        {
            if (flag == true)
            {
                flag = false;
                setFilePath(message.Content);
                setFileOffset(getFileOffest());
                fileStream = getFileStream(path);
                startReadFile();
            }
            else
            {
                Console.WriteLine("Send file to Dispatcher !");
                Send("Dispatcher", path);
            }
        }

        public void readFile()
        {
            Console.WriteLine("Start processing file : " + path);
            string lineOfText;
            var file = new System.IO.StreamReader(fileStream, System.Text.Encoding.UTF8, true, 128);
            while ((lineOfText = file.ReadLine()) != null)
            {
                // do something with content
            }
            Console.WriteLine("End processing file : " + path);
            flag = true;
            fileStream.Close();
        }

        public Int64 isBusyAgent()
        {
            return getFileSize() - getFilePointerPossition();
        }

        public void startReadFile()
        {
            Thread t = new Thread(readFile);
            t.Start();
        }

        public Int64 getFileSize()
        {
            return fileStream.Length / 1024;
        }

        public Int64 getFilePointerPossition()
        {
            return fileStream.Position / 1024;
        }

        public System.IO.FileStream getFileStream(string path)
        {
            return new System.IO.FileStream(path,
                                            System.IO.FileMode.Open,
                                            System.IO.FileAccess.Read,
                                            System.IO.FileShare.ReadWrite);
        }

        public void setFilePath(String path)
        {
            this.path = path;
        }

        public int getFileOffest()
        {
            Random rdn = new Random();
            return rdn.Next(1, 99);
        }

        public void setFileOffset(int fileOffset)
        {
            this.fileOffset = fileOffset;
        }
    }
}
