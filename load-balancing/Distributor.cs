﻿// Am presupus ca din start sunt 3 procese... acum daca e ok abordarea pot adauga la switch ul ala...


using System;
using System.IO;
using ActressMas;

public class Distribuitor : Agent
{
    
    public override void Act(Message msg)
    {
        Console.WriteLine("Distribuitor object instantiated");
    }

    public override void Setup()
    {
        Console.WriteLine("Distribuitor configuration setup");
        getFilesFromDisk();
    }

    public void getFilesFromDisk()
    {
        string path = @"C:\Users\adrian.aioanei\Desktop\test";
        DirectoryInfo dir = new DirectoryInfo(path);  // DirectoryInfo - metoda pt acces la informatiile despre directoare
        int index = 4;

        foreach (FileInfo flInfo in dir.GetFiles()) // pt fiecare nume din director 
        {
            index++;
            Random rnd = new Random(DateTime.Now.Millisecond);
            int rand_num = index % 4;
            String name = flInfo.Name; 
            String m_path = path + "\\" + name;

            switch (rand_num)
            {
                case 0:
                    Send("ProcessorAgent0", m_path);
                    Console.WriteLine("[Distribuitor] Send file to ProcessorAgent1");
                    break;
                case 1:
                    Send("ProcessorAgent1", m_path);
                    Console.WriteLine("[Distribuitor] Send file to ProcessorAgent2");
                    break;
                case 2:
                    Send("ProcessorAgent2", m_path);
                    Console.WriteLine("[Distribuitor] Send file to ProcessorAgent3");
                    break;
                case 3:
                    Send("ProcessorAgent3", m_path);
                    Console.WriteLine("[Distribuitor] Send file to ProcessorAgent4");
                    break;
                default:
                    break;
            }
        }
    }
}
