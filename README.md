# Multi-agent application for load balancing

`Adrian` -> **Processor Agent**

`George` -> **Dispacer** 

`Marius` -> **Distribuitor**


Ideas:

1. Adi primeste ca parametru de la Marius un path catre un fisier, daca are lansat un thread care deja se ocupa de un fisier ii raspunde ["path_fisier"];
2. Marius gaseste numarul de fisiere dintr-un director care le trimite catre Adi, avand ca parametru path-ul respectiv.
3. Daca George primeste mesaj cum ca procesorul este ocupat, incearca sa-l asigneze unui nou procesor din cele 3 disponibile. Daca nici unul din cele 3 nu este disponibil atunci 
creeaza un nou agent numih heper care o sa se ocupa numai de acest fisier dupa care o sa fie distrus.

Steps to clone this repo and to commit some files:

1. Download scm : https://git-scm.com/downloads
2. git clone git@gitlab.com:adrian.aioanei/Multi-agent-application-for-load-balancing.git
3. open created directory in explorer
4. copy your files (add them to the sln)
5. click dreapta where is .git folder and click Git Bash Here
6. git add .
7. git commit -m "Your message"
8. git push
